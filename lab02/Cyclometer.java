///////////////
///// CSE 002 -Section 310
/////YANYI SONG
//// 09/06/2018
////Lab 02 
///
//
public class Cyclometer {
    	// this code is about cyclometer
   	public static void main(String[] args) {

      int secsTrip1=480;  // input the value of secsTrip1
       int secsTrip2=3220;  //  input the value of secsTrip2
		int countsTrip1=1561;  //  input the value of countsTrip1
		int countsTrip2=9037; //  input the value of secsTrip2
      
    double wheelDiameter=27.0,  // input a variable wheeldiameter and the value of the wheeldiameter
  	PI=3.14159, // input the value of PI
  	feetPerMile=5280,  // input a variable feetpermile and the value of feet per mile
  	inchesPerFoot=12,   // input a variable inchesperfoot and the conversion of inches and foot
  	secondsPerMinute=60;  // input a variable secondsperminutes and the conversion of minutes and seconds
	double distanceTrip1, distanceTrip2,totalDistance;  // input variables distanceTrip1, distanceTrip2, and the total distanceTrip1
     
      System.out.println("Trip 1 took "+
       	     (secsTrip1/secondsPerMinute)+" minutes and had "+
       	      countsTrip1+" counts.");  // print out the nummber of counts and minutes for trip1 
	       System.out.println("Trip 2 took "+
       	     (secsTrip2/secondsPerMinute)+" minutes and had "+
       	      countsTrip2+" counts."); // print out the nummber of counts and minutes for trip2
      
      distanceTrip1=countsTrip1*wheelDiameter*PI;
    	// Above gives distance in inches
    	//(for each count, a rotation of the wheel travels
    	//the diameter in inches times PI)
	distanceTrip1/=inchesPerFoot*feetPerMile; // find the distance of trip 1 
	distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile; // find the distance of trip2
	totalDistance=distanceTrip1+distanceTrip2; // find the total distance 
	

      
  System.out.println("Trip 1 was "+distanceTrip1+" miles");// print out the distance of trip1 in miles
	System.out.println("Trip 2 was "+distanceTrip2+" miles");// print out the distance of trip2 in miles 
	System.out.println("The total distance was "+totalDistance+" miles"); // print out the distance for the two trips in miles 
	}  //end of main method   
} //end of class

