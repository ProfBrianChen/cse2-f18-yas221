///////////////
///// CSE 002 -SECTION 310
///////HOMEWORK08
////hw08
/////YANYI SONG
//// 11/14/2018
///

import java.util.Scanner; // import the scanner
import java.util.Random;//import the random 
public class hw08{  // this class is about shuffing 
public static void main(String[] args) { 
Scanner scan = new Scanner(System.in);  // construct the main method
 String[] suitNames={"C","H","S","D"};      //suits club, heart, spade or diamond 
String[] rankNames={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"}; //different rank name
String[] cards = new String[52]; 
String[] hand = new String[5]; 
int numCards = 5; 
int again = 1; 
int index = 51;
for (int i=0; i<52; i++){ 
  cards[i]=rankNames[i%13]+suitNames[i/13]; 
  System.out.print(cards[i]+" "); 
} 
System.out.println();
printArray(cards); 
shuffle(cards); 
printArray(cards); 
while(again == 1){ 
   hand = getHand(cards,index,numCards); 
   printArray(hand);
   index =index - numCards;
   System.out.println("Enter a 1 if you want another hand drawn"); 
   again = scan.nextInt(); 
}  
  } 
  
  public static void printArray( String [] cards){ // the printarray method 
    for (int i=0; i< cards.length; i++){ // out print a deck of cards 
     System.out.print(cards[i]+" "); 
    }
    System.out.println();
  }
  
  public static String[] shuffle(String[]cards){ // the shuffle method 
    Random r = new Random();
    for (int i = 0; i < 52; i++) { 
    int j = r.nextInt(cards.length); // Get a random number from 0-52
     String temp = cards[i]; // create a temp value for the swapping process
      cards[i]=cards[j];
      cards[j]=temp;
    }
    return cards; //return the cards to the main method 
}
  
  public static String [] getHand(String []lists, int index, int numCards){ // the gethand method
    String[] suitNames={"C","H","S","D"};      //suits club, heart, spade or diamond 
String[] rankNames={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"}; //different rank name
String[] cards = new String[52];  // create a deck of cards
    String[] hand = new String[numCards];  // create a hand of cards
    if(numCards>index+1){
     for (int k = 0; k<52;k++){
       cards[k]=rankNames[k%13]+suitNames[k/13];
}
     shuffle(cards); // call the shuffle method again 
     index = 51;
   }
   for(int i = numCards-1; i>=0;i--){
     hand[i] = lists[index - 4 + i];
   }
   return hand; // return the hand to the main method 
 }

  }
  
  
    

