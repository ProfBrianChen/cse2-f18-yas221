///////////////
///// CSE 002 -SECTION 310
///////HOMEWORK09
////remove elements
/////YANYI SONG
//// 11/27/2018

import java.util.Scanner; 
public class RemoveElements {
  
  // this class is about remove elements
  public static int[] randomInput(int num[]) { //the randomInput method
    int i = 0;
    for (i = 0; i < num.length; i++) {    
      num[i] = (int) (Math.random() * 10);  // assign random integars between 0-9 to each element in the array
    }
    return num; 
  }
  
  public static int[] delete(int[] list, int pos) { // the delete method
    int[] newlist = new int[9];  //instruct a new array which has 9 elements
   // int i = 0;
  
    for (int i = 0; i < list.length; i++) { 
      if (i < pos) { //before the entered index position 
      newlist[i] = list[i]; // everything stays the same 
      }
      else if (i > pos) { // after the target index position
      newlist[i - 1] = list[i]; // reassign the new value to each elements 
      }
    }
    return newlist; // return the new list after removing the target index number 
  }
  
  public static int[] remove(int[] list, int target) { // the remove method 
    int length = 10; 
    int i = 0; 
    int j = 0; 
   
    for (i = 0; i < list.length; i++) {
      if (list[i] == target) { 
        length--;
      }
    }
    
    int[] newlist = new int[length];
     
    for (i = 0, j = 0; i < list.length; i++) {
      if (list[i] == target) {  
        continue; 
      }
      else {
        newlist[j] = list[i]; //assign the values from the original array to the new one
        j++;
      }
    }
    return newlist;// return the new array 
    
  }

 public static void main(String [] arg){
  //declare and construct the scanner instance
Scanner scan = new Scanner(System.in);
    //declare and allocate the new array
int num[] = new int[10]; 
int newArray1[]; //the new array after one member is removed
int newArray2[]; //the new array after one member is removed and the elements that have the same value with the target are removed 
int index,target;
int i = 0;
  String out1;
String answer="";
do{
 System.out.print("Random input 10 ints [0-9]"); 
    //calling the method that generates 10 ramdom integers
 num = randomInput(num); 
 String out = "The original array is:";
    //printing out the original array plus all the elements
 out += listArray(num);
 System.out.println(out);
    //prompts the user to input the index
 System.out.print("Enter the index ");
 index = scan.nextInt();
    //checking the whether the index is within the range
    if (index >= 0 && index <= 9) {
 newArray1 = delete(num,index); //calling the method that deletes the one member of the array
    System.out.println("Index " + index + " element is removed");
    out1="The output array is ";
    out1+=listArray(newArray1); //return a string of the form "{2, 3, -9}"  
    System.out.println(out1);
    }
    else {
      //if it is out of the range, print out the original array without changing
      newArray1 = num;
      System.out.println("The index is not valid.");
      out1="The output array is ";
      out1+=listArray(newArray1); //return a string of the form "{2, 3, -9}"  
      System.out.println(out1);
    }
    //////
 
    //prompts the user to enter a target value
    System.out.print("Enter the target value ");
 target = scan.nextInt();
    //checking whether the target exists in the array
    for (i = 0; i < num.length; i++) {
      if (num[i] == target) { //if one element equals to the target, break the for loop
        newArray2 = remove(num,target);
        System.out.println("Element " + target + " has been removed");
        String out2="The output array is ";
        out2+=listArray(newArray2); //return a string of the form "{2, 3, -9}"  
        System.out.println(out2);
        break;
      }
      if (num[i] != target) {
        if (i == num.length - 1) {
        System.out.println("Element " + target + " was not found."); 
        String out2="The output array is ";
        out2+=listArray(newArray1); //return a string of the form "{2, 3, -9}"  
        System.out.println(out2);
        }
        continue;
      }
    }
    /////////
 System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
 answer=scan.next(); //user inputs the answer
}
    while(answer.equals("Y") || answer.equals("y"));
  }
  //adding the punctuations to the print out of the array members
  public static String listArray(int num[]){ 
String out="{";
for(int j=0;j<num.length;j++){
 if(j>0){
   out+=", ";
 }
 out+=num[j];
}
out+="} ";
return out;
  }
  
}