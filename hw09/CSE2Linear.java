///////////////
///// CSE 002 -SECTION 310
///////HOMEWORK09
////cse2linear
/////YANYI SONG
//// 11/27/2018

import java.util.Random;  
import java.util.Scanner;
public class CSE2Linear{// this class is about the linear search function 

  public static void main(String[] args) { // the main method
  
    Scanner myScanner = new Scanner(System.in); // instruct the scanner 
    int i = 0;
  
    int[] finalgrade = new int[15]; // set up an new array
    
    System.out.println("Enter 15 ascending ints for final grades in CSE2: "); // ask for 15 integers fot the array
    for (i = 0; i < 15; i++) {
      
      while (!myScanner.hasNextInt()) { // if the input is not an integar
        System.out.println("There is an Error");
        System.out.println("Please enter integers: ");
        myScanner.next();
        continue;
      }
    finalgrade[i] = myScanner.nextInt();
     
      while (finalgrade[i] > 100 || finalgrade[i] < 0) { // if the integers are not in the range
        System.out.println("There is an Error");
        System.out.println("The integars is not within the range, Please enter integers between 0 to 100: ");
        finalgrade[i] = myScanner.nextInt();
        continue;
      }
     
      if (i >= 1) { // if the latter one is smaller than the previous one 
        while (finalgrade[i] < finalgrade[i - 1]) {
          System.out.println("There is an Error");
          System.out.println("The integers is not valid.Please enter an integer greater or equal to the last one: ");
          finalgrade[i] = myScanner.nextInt();
          continue;
        } 
      
      }
      
    }
   
    for (i = 0; i < 15; i++) {
      System.out.print(finalgrade[i] + " ");
    }
    System.out.println();
 
    System.out.print("Enter a grade to search for:  "); //enter the target grade 
    int targetgrade = myScanner.nextInt();
    
    System.out.println(targetgrade + " was " + binarysearch(finalgrade, targetgrade) ); //call the binary search method 
    
    System.out.println("Scrambled: "); 
   
    Scramble(finalgrade);// call the scramble method 
    System.out.println();
  
    System.out.println("Enter a grade to search for:  ");
    targetgrade = myScanner.nextInt();
  
    System.out.println(targetgrade + " was " + LinearSearch(finalgrade, targetgrade) ); // call the linear search method
   
  }
    public static String binarysearch(int[] finalgrade, int targetgrade){  // the  binary search method
    int lowbound = 0; //input the lowbound
    int highbound = 14; // input the highbound
    int count = 0;//input the iteration
    while (highbound >= lowbound) { // if the highbound is greater or equal to the lowbound 
      count++; 
      int middle = Math.round((lowbound + highbound)/2); // calculate the middle index
      if (finalgrade[middle] == targetgrade) {  // if the target grade is at the middle index  
        return "found in the list with " + count + " iterations";  } //return the result to the main method   
         else if (finalgrade[middle] > targetgrade) { //if  target grade is  located at the lower range
          highbound = middle - 1; //change the higherbound
        }
          else if (finalgrade[middle] < targetgrade) { //if the grade is located at the upper range
            lowbound = middle + 1;}}// change the lowerbound
 return "not found in the list with " + count + " iterations";
    
    }
    
  
    public static String LinearSearch(int[] finalgrade, int targetgrade){ // the linear search method 
    int i = 0; 
    int count = 0; 
  for (i = 0; i < finalgrade.length; i++) { //examine each element in the array
    count++;
    if (finalgrade[i] == targetgrade) { // when the target grade meets one of the element
    return "found in the list with " + count + " iterations";
    }
  }
  
    return "not found in the list with " + count + " iterations";//  when the target grade meets none of the element
  }
  
  
    

  public static void Scramble(int[] finalgrade) { // the scramble method 
  
    Random scramble = new Random(); // the random instructor 
    int i = 0;
    for (i = 0; i < finalgrade.length; i++) { 
    
      int target = (int) ( finalgrade.length * Math.random() );
   
      int swapvalue = finalgrade[target]; 
        finalgrade[target] = finalgrade[i];
       finalgrade[i] = swapvalue;
    }

    for (i = 0; i < 15; i++) { 
      System.out.print(finalgrade[i] + " ");
    }
    
  }
    
}

