///////////////
///// CSE 002 -SECTION 310
///////HOMEWORK02
////ARITHMETIC
/////YANYI SONG
//// 09/10/2018
///

public class Arithmetic {
  
  public static void main(String args[]){
    
              
int numPants = 3;  //Number of pairs of pants
double pantsPrice = 34.98; //Cost per pair of pants

int numShirts = 2;  //Number of sweatshirts
double shirtPrice = 24.99; //Cost per shirt

int numBelts = 1; //Number of belts
double beltCost = 33.99; //cost per belt

double paSalesTax = 0.06; //the pa tax rate

double totalCostOfPants;   //total cost of pants
    double totalCostOfShirts;   //total cost of shirts
    double totalCostOfBelts;   //total cost of belts
    double totalCostOfTaxes; // total sales tax
    
       double salesTaxOnPants; // sales tax charged on pants
        double salesTaxOnShirts; // sales tax charged on shirts
    double salesTaxOnBelts; // sales tax charged on belts
    
    double totalCostBfTax; //	Total cost of purchases before tax
    double totalSalesTax; // total sales tax 
    double totalTransaction; // Total paid for this transaction, including sales tax
    
   totalCostOfPants = numPants * pantsPrice; //calculate the total of cost of pants
    totalCostOfShirts = numShirts * shirtPrice; //calculate the total of cost of shirts
    totalCostOfBelts = numBelts * beltCost; // calculate the total of cost of belts
    salesTaxOnPants = totalCostOfPants * paSalesTax;  // calculate sales tax charged on pants
    salesTaxOnShirts = totalCostOfShirts * paSalesTax; // calculate sales tax charged on shirts
    salesTaxOnBelts = totalCostOfBelts * paSalesTax; //calculate sales tax charged on belts
    totalCostBfTax = totalCostOfPants + totalCostOfShirts + totalCostOfBelts; // calculate total cost of purchases before tax
    totalSalesTax = salesTaxOnPants + salesTaxOnShirts + salesTaxOnBelts; // calculate total sales tax 
    totalTransaction = totalCostBfTax + totalSalesTax; // calculate the Total paid for this transaction, including sales tax
    
     System.out.println("The total cost of pants is "+ totalCostOfPants); // print out the total cost of pants
    System.out.println("The total cost of shirts is "+ totalCostOfShirts); // print out the total cost of shirts
    System.out.println("The total cost of belts is "+ totalCostOfBelts); // print out the total cost of belts
    System.out.printf("The sales tax charged on pants is "+"%.2f\n", salesTaxOnPants); // print out the sales tax charged on pants
        System.out.printf("The sales tax charged on shirts is "+" %.2f\n",salesTaxOnShirts); // print out the sales tax charged on shirts
        System.out.printf("The sales tax charged on belts is "+" %.2f\n",salesTaxOnBelts); // print out the sales tax charged on belts
      System.out.println("The total cost before tax is "+totalCostBfTax); // print out the total cost before tax
        System.out.printf("The total sales tax is "+"%.2f\n",totalSalesTax); // print out the total sales tax 
      System.out.printf("The total transaction is "+"%.2f\n",totalTransaction); // print out the total transaction
    
    
    
  }
}
