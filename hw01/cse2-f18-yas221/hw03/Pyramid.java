///////////////
///// CSE 002 -SECTION 310
///////HOMEWORK03
////PYRAMID
/////YANYI SONG
//// 09/17/2018
///

import java.util.Scanner;
//use this line to enable scanner

public class Pyramid{
    			// THIS LAB IS ABOUT PYRAMID
   			public static void main(String[] args) {
          
          Scanner myScanner = new Scanner( System.in ); //scanner constructer 
          
          System.out.print("Enter the length of square side of the pyramid: "); // input the length of square side of the pyramid
           
          int lengthOfSquare = myScanner.nextInt(); //input the length of square side of the pyramid
          
           System.out.print("Enter the height of the pyramid: " ); // input the height of the pyramid
                 
          int heightOfPyramid = myScanner.nextInt(); // input the height of the pyramid
          
         int volumeInsidePyramid = (lengthOfSquare * lengthOfSquare * heightOfPyramid) / 3; // calculate the volume of the pyramid
          
           System.out.println("The volume inside the pyramid is: " + volumeInsidePyramid); // output the volume of the pyramid 
        }
}
          