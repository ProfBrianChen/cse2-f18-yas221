///////////////
///// CSE 002 -SECTION 310
///////HOMEWORK06
////EncryptedX
/////YANYI SONG
//// 10/21/2018
///

import java.util.Scanner;
//use this line to enable scanner

public class EncryptedX{
    			// THIS LAB IS ABOUT encryptedx
   			public static void main(String[] args) {
          Scanner myScanner = new Scanner( System.in ); //scanner constructer
          System.out.print("enter an integer between 1-100:"); // ask for the input 
    boolean response = true;
        int numRows=0;
          
          while (response){      
            if(myScanner.hasNextInt()){  // check the input is an integar 
              numRows = myScanner.nextInt();
            
            
            
            if(numRows<1||numRows>100){  // check in the input is within the range or not 
              System.out.println("it's not in the range,enter an integar between 1-100 again");
            }
            else {
              response = false;
            }
            }
            else {
              myScanner.next(); //ask for the input again 
              System.out.println("not an integer, enter an integar between 1-100 again");
            }
          }
          //the nested loop
          int r; //rows
          int nn;  //column 
   for (r = 1;  r<= numRows; r++) {
   for (nn = 1; nn <= numRows; nn++) {
   if (r==nn){System.out.print(" ");} //indicate the first situation where * should not be displayed
     else if(nn==numRows-(r-1)) {System.out.print(" ");} ///indicate the second situaton where * should not be displayed
    else {System.out.print("*");} // out put the * location 
   }
   System.out.println(); //outprint the entire pattern
   }
}
}
//end of the program

