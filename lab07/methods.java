///////////////
///// CSE 002 -Section 310
/////YANYI SONG
//// 10/25/2018
////Lab 07
///Methods
//
import java.util.Scanner;
import java.util.Random;
public class methods {
   public static void main(String[] args) {             //the main method
    System.out.println(sentences());                   
  } 
     
  
  public static String adjectives () {            //Method for adjectives
    Random randomGenerator = new Random();        
    int randomInt = randomGenerator.nextInt(10);  // generate an integer between 0 to 9
    String adjective = " ";
    switch (randomInt) {                         
      case 0:
        adjective = "beautiful";
        break;
      case 1:
        adjective = "clever";
        break;
      case 2:
        adjective = "happy";
        break;
      case 3:
        adjective = "sad";
        break;
      case 4:
        adjective = "good";
        break;
      case 5:
        adjective = "strong";
        break;
      case 6:
        adjective = "tired";
        break;
      case 7:
        adjective = "kind";
        break;
      case 8:
        adjective = "sweet";
        break;
      case 9:
        adjective = "cool";
        break;
    }
    return adjective;  
  }
  
 
  
  public static String nounSubjects () {            //method for the subjects
    Random randomGenerator = new Random();          
    int randomInt = randomGenerator.nextInt(10);   
    String nounSubject = " ";
    switch (randomInt) {
      case 0:
        nounSubject = "flora";
        break;
      case 1:
        nounSubject = "iris";
        break;
      case 2:
        nounSubject = "laura";
        break;
      case 3:
        nounSubject = "doria";
        break;
      case 4:
        nounSubject = "pig";
        break;
      case 5:
        nounSubject = "ming";
        break;
      case 6:
        nounSubject = "olivia";
        break;
      case 7:
        nounSubject = "dog";
        break;
      case 8:
        nounSubject = "cat";
        break;
      case 9:
        nounSubject = "elephant";
        break;
      }
      return nounSubject;
  }
    
  
    public static String pastVerbs () {            // method for the past tense verb 
    Random randomGenerator = new Random();        
    int randomInt = randomGenerator.nextInt(10);   //randomly generate an integer between 0 to 9
    String pastVerb = " ";
    switch (randomInt) {
      case 0:
        pastVerb = "ran";
        break;
      case 1:
        pastVerb = "jumped";
        break;
      case 2:
        pastVerb = "accepted";
        break;
      case 3:
        pastVerb = "thought";
        break;
      case 4:
        pastVerb = "visited";
        break;
      case 5:
        pastVerb = "left";
        break;
      case 6:
        pastVerb = "yelled";
        break;
      case 7:
        pastVerb = "sat";
        break;
      case 8:
        pastVerb = "spent";
        break;
      case 9:
        pastVerb = "stood";
        break;
      }
      return pastVerb;
  }
  
  public static String nounObjects () {             //method for the objects
    Random randomGenerator = new Random();          
    int randomInt = randomGenerator.nextInt(10);    //generate an integer between 0 to 9
    String nounObject = " "; 
    switch (randomInt) {
      case 0:
        nounObject = "cup";
        break;
      case 1:
        nounObject = "apple";
        break;
      case 2:
        nounObject = "pen";
        break;
      case 3:
        nounObject = "name";
        break;
      case 4:
        nounObject = "ball";
        break;
      case 5:
        nounObject = "cake";
        break;
      case 6:
        nounObject = "piano";
        break;
      case 7:
        nounObject = "book";
        break;
      case 8:
        nounObject = "mirror";
        break;
      case 9:
        nounObject = "water";
        break;
      }
      return nounObject;
  }

 
    public static String thesis() {                           // method for the first sentence
    
    Scanner myScanner = new Scanner(System.in);               //declare and construct Scanner instance 
    String nSub;                                           
    while (true) {                                            //this while loop will keep running until it is not true
      
    String adj1 = adjectives();                              
    String adj2 = adjectives(); 
    String adj3 = adjectives();
    nSub = nounSubjects();                                
    String pastV = pastVerbs();                               
    String nObj = nounObjects();                              
    String thesisSen = "The " + adj1 +" "+ adj2 + " " + nSub + " " + pastV + " " + "the " + adj3 + " " + nObj + "."; // the first sentence also the thesis
    System.out.println (thesisSen);  // construct the first sentence
    System.out.println ("Would you like another sentence? please type in 1 for yes or 2 for no");  // ask user whether he or she wants another sentence
    boolean response = true;
      int answer=0;
      
      while (response){
        if(myScanner.hasNextInt()){
          answer = myScanner.nextInt();
        
        if(answer<1||answer>2){
          System.out.println("it is not in the range,enter a value again");
        }
        else{
          response = false;
          
        }
      }
        else{
          myScanner.next();
          System.out.println("not an integar, please enter again");    
        }
      }
      
     
      if (answer==1) {    // if the user types in "yes" 
        continue;                      // 
      }
        else {                         // if users enter no
          break;                       
        }
    }   
        return nSub;                   // return the subject 
    }
  
    public static String sentences () {          // method for paragraph
    Scanner myScanner = new Scanner(System.in); 
    String adj4 = adjectives();               
    String adj5 = adjectives(); 
 String adj6 = adjectives();  
      String adj7 = adjectives();  
       String adj8 = adjectives(); 
       String adj9 = adjectives();  
      
    String nSub = thesis();     
    String pastV = pastVerbs();  
         String pastV1 = pastVerbs();  
    String nObj = nounObjects(); 
     String nObj1 = nounObjects(); 
        String nObj2 = nounObjects(); 
        String nObj3 = nounObjects();
        String nObj4 = nounObjects();
    String sen1 = "This " + nSub + " was " + adj4 + " " + adj5 + " to " + adj6 + " " + nObj + "."; 
    String sen2 = nSub + " used " + nObj1 + " to " + pastV + " " + nObj2 + " at the " + adj8 + " " + nObj3 + "."; 
    String conclusion = "That " + nSub + " " + pastV1 + " her " + nObj4 + "."; 
    
    return (sen1 + "\n" + sen2 + "\n" + conclusion);  
  }  
  }